#------------------------------------- NIR-Cal Data Calibration Pipeline -------------------------------------#
#b.j.clark@keele.ac.uk - v1.06 (02/11/2017)
#------------------------------------------

############ USER VARIABLES ############
direc = "/data/1_imaging/cfht/w36/1.raw/" #directory containin all raw data (including calibration images)
output_dir="w36_cal" #name of output directory
newdir = False #create a new directory for outputs
master_table = "datamaster.cfht" #output data log name
nstars = 20 #maximum number of stars for detection algorithm
row_correction=False#Correct Bad row values (darker streaks) - DANGER: think this does bad stuff
debug_short_test=False #only reduce 10 science images if True
sci_ob="WASP-36"#the  target object keywork
skipimcor=False#skip science image correction if already performed
ap_size_lim=200 #minimum npix for valid aperture
ap_mult=2#amount to multiply mean aperture radius for "aperture" pixels
sigma_psf = 4#amount bg pix clip from median
show_sources=False
remove_bad = True#removes psfs that contain nans

############ LIBRARIES ############
from standard import *
import pandas
from astropy.time import Time
import pyfits

############ GLOBAL VARIABLES ############
global dataset #pandas dataframe for file logging
global index_master #index in pandas table

prihdr("Data Calibration Pipeline")

############ CLASSES ############
class cfht_img:
	#class to handle reading and writing while preserving CFHT data structure
	def __init__(self,fl):
		self.fl = fl
		self.fits = pyfits.open(fl)
		self.extensions = len(self.fits)
		self.imgs, self.hdrs = [], []
		for ext in range(0,self.extensions):
			Ext = self.fits[ext]
			Hdr = Ext.header
			Data = numpy.asarray(Ext.data,dtype="float")
			structure = Data.shape
			if len(structure) == 3:
				self.frames = structure[0]
				sub_imgs,sub_hdrs = [],[]
				for frm in range(0,self.frames):
					sub_imgs.append(numpy.asarray(Ext.data[frm],dtype="float"))
					sub_hdr = Hdr.copy()
					sub_hdr["MJD-OBS"] = float(sldate_mjd(Hdr,frm))
					sub_hdrs.append(sub_hdr)
				self.imgs.append(numpy.asarray(sub_imgs))
				self.hdrs.append(sub_hdrs)
			else:
				self.frames = 1
				self.imgs.append(Data)
				self.hdrs.append(Hdr)

	def write(self,fname,clobber=False):
		hdu = pyfits.PrimaryHDU()
		hdu.header = self.hdrs[0]
		hdus = [hdu]
		for i in range(1,self.extensions):
			hdu = pyfits.ImageHDU()
			hdu.data = self.imgs[i]
			try:
				self.hdrs[i]["BITPIX"]
			except TypeError:
				hdu.header = self.hdrs[i][0].copy()
			else:
				hdu.header = self.hdrs[i].copy()
			hdu.header["BZERO"] = 1
			hdus.append(hdu)
		img_out = pyfits.HDUList(hdus)
		img_out.writeto(fname,clobber=clobber)

	def write_frames(self,fname,frame="all",clobber=False):
		fbase = fname.split(".fits")[0]
		for f in range(self.frames):
			if f != frame and frame != "all":
				continue
			hdu = pyfits.PrimaryHDU()
			hdu.header = self.hdrs[0]
			f=int(f)
			try:
				if int(self.frames) == 1:
					hdu.header['MJD-OBS'] = self.hdrs[1]['MJD-OBS']
				else:
					hdu.header['MJD-OBS'] = self.hdrs[1][f]['MJD-OBS']
			except TypeError:
				print "FRAME:",f
				print fname
				print self.fl
				print "\n\nMJD:,", hdu.header['MJD-OBS']
				print "\n\n"
				print len(self.hdrs[1])
				raise
				sys.exit()
			hdus = [hdu]
			for i in range(1,self.extensions):
				hdu = pyfits.ImageHDU()
				if int(self.frames) == 1:
					hdu.data = self.imgs[i]
					hdu.header = self.hdrs[i].copy()
				else:
					hdu.data = self.imgs[i][f]
					hdu.header = self.hdrs[i][f].copy()
				hdu.header["BZERO"] = 1
				hdus.append(hdu)
			img_out = pyfits.HDUList(hdus)
			if not os.path.exists(fbase+"_%02d"%f+".fits"):
				img_out.writeto(fbase+"_%02d"%f+".fits",clobber=clobber)

############ FUNCTIONS ############
def norml(array):
	return array/median(array)

def sldate_mjd(header, frame,mjd=True):
	utc = Time(header["SLDATE"+"%02d"%(frame+1)])
	if mjd==True:
		return utc.mjd
	else:
		return utc.jd


def find_closest_exp(tablobj, exp):
	exps = numpy.asarray(tablobj.exp.unique())
	return exps[zip(*sorted(zip(numpy.abs(exp-exps),range(0,len(exps),))))[1][0]]


def darkcombine(images,output,combine="median",clobber=False):
	to_combine = {}
	for ext in range(1,5):
		to_combine[ext] = []
	for IM in images:
		for ext in range(1,len(IM.imgs)):
			for frame in range(0,len(IM.imgs[ext])):
				to_combine[ext]
				IM.imgs[ext][frame]
				to_combine[ext].append(IM.imgs[ext][frame])
	out = []
	for ext in to_combine:
		if combine == "median":
			out.append(numpy.nanmedian(to_combine[ext],axis=0))
		if combine == "mean":
			out.append(numpy.nanmean(to_combine[ext],axis=0))
	hdu = pyfits.PrimaryHDU()
	hdu.header = IM.hdrs[0]
	hdu.header["OBSTYPE"] = "MASTERDARK"
	hdus = [hdu]
	for i in range(1,5):
		hdu = pyfits.ImageHDU()
		hdu.data = out[i-1]
		hdu.header = IM.hdrs[i][0]
		hdu.header["BZERO"] = 0
		hdu.header["OBSTYPE"] = "MASTERDARK"
		hdus.append(hdu)
	img_out = pyfits.HDUList(hdus)
	img_out.writeto(output,clobber=clobber)

def flatcombine(images,masterdark,output,combine="median",scale="median",clobber=False):
	to_combine = {}
	for ext in range(1,5):
		to_combine[ext] = []
	for IM in images:
		for ext in range(1,5):
				to_combine[ext].append(IM.imgs[ext])
	out = []
	for ext in to_combine:
		img_set = numpy.asarray(to_combine[ext],dtype="float") - masterdark.imgs[ext]
		if scale == "median":
			img_med = numpy.nanmedian(img_set)
			img_set = [x*(img_med/numpy.nanmedian(x)) for x in img_set]
		elif scale == "mean":
			img_med = numpy.nanmean(img_set)
			img_set = [x*(img_med/numpy.nanmean(x)) for x in img_set]
		if combine == "median":
			out.append(norml(numpy.nanmedian(img_set,axis=0)))
		elif combine == "mean":
			out.append(norml(numpy.nanmean(img_set,axis=0)))
		else:
			raise ValueError("Need a method to combine images..")
	hdu = pyfits.PrimaryHDU()
	hdu.header = IM.hdrs[0]
	hdu.header["OBSTYPE"] = "MASTERFLAT"
	hdus = [hdu]
	for i in range(1,5):
		hdu = pyfits.ImageHDU()
		hdu.data = out[i-1]
		hdu.header = IM.hdrs[i]
		hdu.header["BZERO"] = 0
		hdu.header["OBSTYPE"] = "MASTERFLAT"
		hdus.append(hdu)
	img_out = pyfits.HDUList(hdus)
	img_out.writeto(output,clobber=clobber)
	return(cfht_img(output))

def nonlincor(adu,det):
	#nonlinear corrections, !!specific to CFHT!!
	allcoeffs = ([0.991276,1.72141e-6,7.57226e-12],[0.993746,1.82717e-6,1.9395e-11],[0.994254,1.9277e-6,1.69437e-11],[0.994799,1.49037e-6,2.85603e-11])
	coeffs = allcoeffs[det]
	return adu * (coeffs[0] + (coeffs[1]*adu) + (coeffs[2]*adu*adu))

def row_correct(im):
	med_x = numpy.nanmedian(im,axis=0)
	im2=im.copy()
	im2[:]=[im[i,:]/med_x for i in range(0,len(im[1]))]
	im2=im2*numpy.nanmedian(med_x)
	return im2

############ MAIN ############
index_master = 0
output_dir = cmdir(output_dir,fp=True,makenew=newdir)
master_table = output_dir + master_table
columns=["file","exp","typ"]
try:
	#check for saved progress
    dataset = pandas.read_pickle(master_table)
    print "\nReading data from saved table.."
except IOError:
    raw_images = file_list_wext(direc,".fits")
    if debug_short_test==True:
    	raw_im_sci = [i for i, v in enumerate(raw_images) if "o.fits" in v]
    	raw_im_cal = [i for i, v in enumerate(raw_images) if "o.fits" not in v]
    	raw_im_sci=nar(raw_images)[raw_im_sci]
    	raw_im_cal=nar(raw_images)[raw_im_cal]
        imglen = int(math.floor(len(raw_im_sci)/10))
        raw_im_sci=raw_im_sci[::imglen]
        raw_images = raw_im_cal.tolist()+raw_im_sci.tolist()
    	print "WARNING: DEBUG MODE IS ON, NOT REDUCING ALL IMAGES"
    dataset = pandas.DataFrame(columns=columns)
    print "\nReading data from fits.."
    bar = pbar(len(raw_images))
    counter = 0
    object_names=[]
    for fl in raw_images:
        data = pyfits.open(direc+fl)
        dataset.loc[index_master] = [direc+fl,data[1].header["EXPTIME"],data[1].header["OBSTYPE"]]
        if data[1].header["OBSTYPE"] != "DARK" and data[1].header["OBSTYPE"] != "FLAT":
        	object_names.append(data[1].header["OBJECT"])
        index_master += 1
        bar.update(counter)
        counter+=1
    bar.finish()
    object_names=numpy.unique(object_names)
    if len(object_names) > 1:
    	print "Mutiple objects detected:"
    	c1=0;
    	for ob in object_names:
    		c1+=1
    		print str(c1)+" - "+ob
    	sci_ob = object_names[int(input("Please enter science object:"))-1]
    else:
    	sci_ob = object_names[0]
    print "Science Object:",sci_ob
    dataset.to_pickle(master_table)

#dark correction
print "\nCreating master darks.."
masterdarks = dataset.loc[dataset['typ'] == "MASTERDARK"]
darks = dataset.loc[dataset['typ'] == "DARK"]
bar = pbar(len(darks.exp.unique()));cnt = 0;
for exp in darks.exp.unique():
	bar.update(cnt); cnt += 1;
	if exp in masterdarks.exp.unique():continue;
	darklist = [cfht_img(x) for x in darks.loc[darks.exp == exp].file.tolist()]
	md_out = cmdir(output_dir+"masterdarks/") + "master_dark_"+str(exp)+".fits"
	darkcombine(darklist, md_out,clobber=True)
	dataset.loc[index_master] = [md_out,exp,"MASTERDARK"]
	index_master += 1
dataset.to_pickle(master_table)
bar.finish()
masterdarks = dataset.loc[dataset['typ'] == "MASTERDARK"]

#flat correction
masterflats = dataset.loc[dataset['typ'] == "MASTERFLAT"]
print "\nCreating master flats.."
flats = dataset.loc[dataset['typ'] == "FLAT"]
bar = pbar(len(flats.exp.unique()));cnt = 0;
for exp in flats.exp.unique():
	bar.update(cnt); cnt += 1;
	if exp in masterflats.exp.unique():continue;
	flatlist = [cfht_img(x) for x in flats.loc[flats.exp == exp].file.tolist()]
	mf_out = cmdir(output_dir+"masterflats/") + "master_flat_"+str(exp)+".fits"
	md = cfht_img(masterdarks.loc[masterdarks['exp'] == exp].file.tolist()[0])
	flatcombine(flatlist, md, mf_out,clobber=True)
	dataset.loc[index_master] = [mf_out,exp,"MASTERFLAT"]
	index_master += 1
dataset.to_pickle(master_table)
bar.finish()
masterflats = dataset.loc[dataset['typ'] == "MASTERFLAT"]

#bad pixel map creation
masterbpmap = dataset.loc[dataset['typ'] == "BADPIXMAP"]
for i in masterflats.index:
	fits = cfht_img(masterflats.loc[i].file)
	for ext in range(1,fits.extensions):
		fits_img = fits.imgs[ext]
		cut_hi = median(fits_img) + (5 * med_abs_dev(fits_img))
		cut_lo = median(fits_img) - (5 * med_abs_dev(fits_img))
		fits_img[numpy.where(fits_img>cut_hi)] = numpy.nan
		fits_img[numpy.where(fits_img<cut_lo)] = numpy.nan
		fits_img[~numpy.isnan(fits_img)] = 0
	bp_out = cmdir(output_dir+"badpixmap/") + "bad_pix_map_"+str(masterflats.loc[i].exp)+".fits"
	fits.write(bp_out,clobber=True)
	dataset.loc[index_master] = [bp_out,masterflats.loc[i].exp,"BADPIXMAP"]
	index_master += 1
plt.show()
masterbpmap = dataset.loc[dataset['typ'] == "BADPIXMAP"]

if debug_short_test == True:
	raw_input("Science Correction Starting - enter to continue")

#raw image correction
science = dataset.loc[dataset['typ'] == "SCIENCE"]
print "\nCorrecting Raw Images.."
raw = dataset.loc[dataset['typ'] == "OBJECT"]
if skipimcor==False:
	bar = pbar(len(raw));cnt = 0;1
	for exp in raw.exp.unique():
		md = cfht_img(masterdarks.loc[masterdarks['exp'] == find_closest_exp(masterdarks,exp)].file.tolist()[0])
		mf = cfht_img(masterflats.loc[masterflats['exp'] == find_closest_exp(masterflats,exp)].file.tolist()[0])
		mbp = cfht_img(masterbpmap.loc[masterbpmap['exp'] == find_closest_exp(masterbpmap,exp)].file.tolist()[0])
		for i in raw.index:
			fits = cfht_img(raw.loc[i].file)
			for ext in range(1,fits.extensions):
				for frm in range(0,fits.frames):
					try:
						#set all saturated pixels to nans
						fits.imgs[ext][frm][numpy.where(fits.imgs[ext][frm]==65536)] = numpy.nan
						#chipbias correction
						fits.imgs[ext][frm] = fits.imgs[ext][frm] - 7000
						#Flag all bad pixels (>36000ADU)
						fits.imgs[ext][frm][numpy.where(fits.imgs[ext][frm]>36001)] = numpy.nan
						#apply bad pixel mask
						fits.imgs[ext][frm] = fits.imgs[ext][frm] * mbp.imgs[ext]
						# show_fits(fits.imgs[ext][frm])
						#Non-Linear Correction
						fits.imgs[ext][frm] = nonlincor(fits.imgs[ext][frm],ext-1)
						#Dark Subtraction
						fits.imgs[ext][frm] = fits.imgs[ext][frm] - md.imgs[ext] + 7000 - 1 #chipbias+dark+bzero_remnant
						#Flat Division
						fits.imgs[ext][frm] = fits.imgs[ext][frm] / mf.imgs[ext]
						#Row correction
						if row_correction==True:
							to_cor = fits.imgs[ext][frm].copy()
							to_cor=to_cor.T
							corrected = row_correct(to_cor)
							fits.imgs[ext][frm]=corrected.T
					except ValueError:
						print "ERROR FOR ", raw.loc[i].file, ext, frm
			sci_out = cmdir(output_dir+"science/") + raw.loc[i].file.split("/")[-1]
			fits.write_frames(sci_out,clobber=True)
			bar.update(cnt); cnt += 1;
	bar.finish()

	fits = get_files(output_dir+"science/",ext="fits")
	print "Organising Images.."
	bar = pbar(len(fits));cnt=0
	for fl in fits:
		bar.update(cnt);cnt+=1
		i,h = fits_import(output_dir+"science/"+fl,dext=1,hext=1)
		dr=cmdir(output_dir+"science/"+h["OBJECT"])
		shutil.move(output_dir+"science/"+fl,dr+fl)
	bar.finish()

# NAN fix for CFHT data
print "Correcting NaNs.."
#-- User Variables ----------------------------------------------------------------
sci_direc=slash(output_dir+"science/"+sci_ob)
print sci_direc
flist = get_files(sci_direc,ext="fits")
#-- Libraries ---------------------------------------------------------------------
from standard import *
import sep
import cv2 
from lmfit import *
from scipy import ndimage, interpolate
from astropy.modeling import models, fitting

#-- Functions ---------------------------------------------------------------------
def CircularMask(img, x,y, radius=10):
	#create a circular mask around a given x,y image
	#inputs image, x_center, y_center, mask radius
	#returns numpy array of mask
	h, w=numpy.shape(img)
	center=[int(y),int(x)]
	Y, X = np.ogrid[:h, :w]
	dist_from_center = np.sqrt((X - center[0])**2 + (Y-center[1])**2)
	mask = dist_from_center <= radius
	return mask

def window(img,x,y,w,ret="i"):
	#create a subimage around x,y with width y
	#return window image
	x,y,w=int(x),int(y),int(w)
	x_lo = x-w
	x_hi = x+w+1
	y_lo = y-w
	y_hi = y+w+1
	if x_lo < 0: x_lo=0;
	if x_hi > len(img): x_hi = len(img)
	if y_lo < 0: y_lo=0;
	if y_hi > len(img[0]): y_hi = len(img[0])
	if ret=="i":
		return (img[x_lo:x_hi,y_lo:y_hi].copy(),(x_lo,x_hi,y_lo,y_hi))
	if ret=="v":
		return(x_lo,x_hi,y_lo,y_hi)

def show_fitsc(img, clims="auto",show=True,newfig=False,bgsub=False,title=False):
  #plot fits image with cut levels defined by clims
  import matplotlib.pyplot as plt
  if newfig == False:
    fig = plt.figure()
    cmap = plt.cm.hot
    cmap.set_bad((0, 0.6, 1, 1))
    ax = fig.add_subplot(111)
  else:
      fig,pos = newfig
      ax = fig.add_subplot(pos)
  if title != False:
    ax.set_title(title)
  if clims == "auto":
    if bgsub == True:
      from scipy.stats import mode
      goodpix = img[~numpy.isnan(img)]
      bad = mode(img[~numpy.isnan(img)])[0][0]
      med = numpy.median(goodpix[numpy.where(goodpix!=bad)])
      mad = med_abs_dev(goodpix[numpy.where(goodpix!=bad)])
    else:
      med = numpy.median(img[~numpy.isnan(img)])
      mad = med_abs_dev(img[~numpy.isnan(img)])
    thrh,thrl = med+(mad*10), med-(mad*10)
    if thrl < min(flatten_list(img.tolist())): thrl = min(flatten_list(img.tolist()))
    if thrh > max(flatten_list(img.tolist())): thrh = max(flatten_list(img.tolist()))
  else:
    thrh=clims[0]
    thrl=clims[1]
  cmap = plt.cm.hot
  im1 = ax.imshow(img.T,clim=(thrl,thrh),origin='lower',interpolation="none",cmap=cmap)
  fig.colorbar(im1)
  numrows, numcols = img.shape
  imFORMAT = img.T
  def format_coord(x, y):
    col = round(x)
    row = round(y)
    if col>=0 and col<numcols and row>=0 and row<numrows:
        z = imFORMAT[int(row),int(col)]
        return 'x=%1.4f, y=%1.4f, count=%1.4f'%(x, y, z)
    else:
        return 'x=%1.4f, y=%1.4f'%(x, y)
  ax.format_coord = format_coord
  plt.tight_layout()
  if show == True:
    plt.show()
  else:
    return ax,(thrl,thrh)

#-- PSF Fitting ---------------------------------------------------------------------
def psf_compare(free_params,psf1,psf2,ret="lsq"):
	# print free_params['x'].value,free_params['y'].value,free_params['factor'].value#debug
	shift = ndimage.interpolation.shift(psf1,(float(free_params['x'].value),float(free_params['y'].value)))
	shift = numpy.asarray(shift)*free_params['factor'].value
	shift[numpy.where(shift<0)]=0
	try:
		res=psf2-shift
	except ValueError:
		res=numpy.asarray([[9999,9999],[9999,9999]],dtype="float")
	if ret == "lsq":
		res = res[~numpy.isnan(res)]
		return res.flatten()
	elif ret == "psf":
		return shift
	else:
		return res

def psf_fix(psf_tofit,psf_match,plot=False):
	if numpy.shape(psf_tofit) != numpy.shape(psf_match):
		psf_match = reshape_psf(psf_tofit,psf_match)
	kx,ky=flux_weighted_centroid(psf_match)
	wx,wy=flux_weighted_centroid(psf_tofit)
	psf_match_gd = psf_match[~numpy.isnan(psf_match)]
	psf_tofit_gd = psf_tofit[~numpy.isnan(psf_tofit)]
	psf_match_gd = psf_match_gd[numpy.where(psf_match_gd!=0)]
	psf_tofit_gd = psf_tofit_gd[numpy.where(psf_tofit_gd!=0)]
	ratio=numpy.nanmean(numpy.asarray(psf_tofit_gd,dtype="float"))/numpy.nanmean(numpy.asarray(psf_match_gd,dtype="float"))
	free_params = Parameters()
	free_params.add('x', value= wx-kx,min=wx-kx-5.01,max=wx-kx+5.01)
	free_params.add('y', value= wy-ky,min=wy-ky-5.01,max=wy-ky+5.01)
	free_params.add('factor', value=ratio,min=0.001,max=2.0)
	if plot==True:
		fig = plt.figure(figsize=(20,20))
		cl,ch = -200,1000
		show_fitsc(psf_tofit,newfig=(fig,231),show=False,title="Original PSF",clims=(cl,ch))
		show_fitsc(psf_match,newfig=(fig,232),show=False,title="PSF Kernel",clims=(cl,ch))
	result = minimize(psf_compare, free_params,args=(psf_match,psf_tofit),method="anneal")
	psf_match = psf_compare(free_params,psf_match,psf_tofit,ret="psf")
	try:
		psf_res = psf_tofit- psf_match
		psf_tofit[numpy.isnan(psf_tofit)] = psf_match[numpy.isnan(psf_tofit)]
	except ValueError:
		return psf_tofit
	if plot == True:
		show_fitsc(psf_match,newfig=(fig,233),show=False,title="Matched kernel",clims=(cl,ch))
		show_fitsc(psf_res,newfig=(fig,234),show=False,title="Residuals",clims=(cl,ch))
		show_fitsc(psf_tofit,newfig=(fig,235),title="Corrected Orig PSF",clims=(cl,ch))
	return psf_tofit

def reshape_psf(psf,kern):
	kern_resize = kern.copy()
	xd = len(psf)-len(kern)
	xl = int(abs(math.floor(xd/2)))
	xh = int(abs(xd) - xl)
	if xd <0:
		kern_resize = kern_resize[xl:-xh,:]
	elif xd >0:
		kern_resize = numpy.insert(kern_resize,[0]*xl,0,axis=0)
		kern_resize = numpy.insert(kern_resize,[len(kern_resize)]*xh,0,axis=0)
	yd = len(psf[0])-len(kern[0])
	yl = int(abs(math.floor(yd/2)))
	yh = int(abs(yd) - yl)
	if yd < 0:
		kern_resize = kern_resize[:,yl:-yh]
	elif yd >0:
		kern_resize = numpy.insert(kern_resize,[0]*yl,0,axis=1)
		kern_resize = numpy.insert(kern_resize,[len(kern_resize[0])]*yh,0,axis=1)
	return kern_resize

def flux_weighted_centroid(img):
    X,Y,flux = [],[],[]
    for y in range(0,len(img)):
        for x in range(0,len(img[0])):
            X.append(x)
            Y.append(y)
            flux.append(img[y][x])
    X,Y,flux = numpy.asarray(Y),numpy.asarray(X),numpy.asarray(flux)
    flux[numpy.where(numpy.isnan(flux))] = 0
    try:
    	return numpy.array((numpy.average(X,weights=flux),numpy.average(Y,weights=flux)))
    except ZeroDivisionError:
    	return (int(len(img)/2),int(len(img[0])/2))


#-- Main --------------------------------------------------------------------------
nan_out = cmdir(output_dir+"nanfix/")
if remove_bad==True:
	ap_mult=1.
bar=pbar(len(flist));cnt=0
for fl in flist:
	bar.update(cnt);cnt+=1
	kernel="test"
	for det in [1,2,3,4]:
		det_out = cmdir(nan_out + "det"+str(det)+"/")
		if os.path.exists(det_out+fl):
			continue
		img,hdr=fits_import(sci_direc+fl,dext=det,hext=det)
		imgin=img.copy()

		#Calculate and subtract background
		img = img.copy(order='C')
		img = img.byteswap().newbyteorder()
		bkg = sep.Background(img)
		img[numpy.isnan(img)] = nar(bkg)[numpy.isnan(img)]
		data_sub = img - bkg

		#PSFs -> gaussian
		data_sub = cv2.GaussianBlur(data_sub,(15,15),0)

		#Extract sources
		objects = sep.extract(data_sub, 5, err=bkg.rms())

		#filter false flags - objects too elliptical
		bad = numpy.where(objects['a']/objects['b']>2)[0]
		objects=numpy.delete(objects,bad)

		#filter false flags - objects too small
		bad = numpy.where(objects['npix']<ap_size_lim)[0]
		objects=numpy.delete(objects,bad)

		#Plot
		if show_sources==True:
			from matplotlib.patches import Ellipse
			fig, ax = plt.subplots()
			m, s = np.nanmedian(imgin), med_abs_dev(imgin)*10
			im = ax.imshow(imgin.T, interpolation='none', cmap='hot',
			               vmin=m-s, vmax=m+s, origin='lower')
			for i in range(len(objects)):
			    e = Ellipse(xy=(objects['y'][i], objects['x'][i]),
			                width=6*objects['a'][i],
			                height=6*objects['b'][i],
			                angle=objects['theta'][i] * 180. / np.pi)
			    e.set_facecolor('none')
			    e.set_edgecolor('cyan')
			    ax.add_artist(e)
			plt.show()

		#get object positions and radii
		x,y =objects['y'], objects['x']
		good = numpy.where(~numpy.isnan(x))
		x,y=x[good],y[good]
		good = numpy.where(~numpy.isnan(y))
		x,y=x[good],y[good]
		apsize = numpy.sqrt(numpy.mean(objects['npix'])/numpy.pi) * ap_mult

		img_flagged = imgin.copy()
		for i in range(0,len(x)):
			try:
				mask=CircularMask(img_flagged,x[i],y[i],apsize)
			except ValueError:
				print x
				print y
				print "FATAL ERROR: Masking Failed [699]"
				sys.exit()
			img_flagged[mask]=987654

		#correct BG pixels
		img_flagged[numpy.isnan(img_flagged)]=nar(bkg)[numpy.isnan(img_flagged)]

		#guider square fix
		med=numpy.nanmedian(img_flagged)
		dev=med_abs_dev(img_flagged)*5
		img_flagged[numpy.where(img_flagged<med-dev)] = nar(bkg)[numpy.where(img_flagged<med-dev)]

		#unmask image
		img_unmask=img_flagged.copy()
		img_unmask[numpy.where(img_unmask==987654)]=imgin[numpy.where(img_unmask==987654)]

		#edge nan correction
		img_unmask[:,:5]=nar(bkg)[:,:5]
		img_unmask[:5,:]=nar(bkg)[:5,:]
		img_unmask[-4:,:]=nar(bkg)[-4:,:]
		img_unmask[:,-4:]=nar(bkg)[:,-4:]

		#PSF nan correction:
		if remove_bad == True:
			img_nancor2=img_unmask.copy()
			for i in range(0,len(x)):
				mask=CircularMask(img_nancor2,x[i],y[i],apsize)
				if True in numpy.isnan(img_nancor2[mask]):
					img_nancor2[mask]=numpy.nan
		else:
			#    get kernel
			img_nancor2=img_unmask.copy()
			if kernel=="test":
				for i in range(0,len(x)):
					X,Y = x[i],y[i]
					win = window(img_unmask,X,Y,apsize*2)[0]
					if len(numpy.where(numpy.isnan(win))[0])!=0:
						continue
					kernel=win-(numpy.nanmedian(win)+(sigma_psf*med_abs_dev(win)))
					kernel[numpy.where(kernel<0)]=0
					break
			if kernel=="test":
				print "ERROR: Could not find good PSF in whole image!"
				sys.exit()

			#    fix bad psf's
			for i in range(0,len(x)):
				X,Y = x[i],y[i]
				win,pos = window(img_unmask,X,Y,apsize*2)
				if len(numpy.where(numpy.isnan(win))[0])==0:
					continue
				X,Y =numpy.array([pos[0],pos[2]],dtype="float")+flux_weighted_centroid(win)
				win,p2 = window(img_unmask,X,Y,apsize*2)
				win_check=win.copy()
				if len(numpy.where(numpy.isnan(win))[0])==0:
					continue
				bgsub=(numpy.nanmedian(win)+(sigma_psf*med_abs_dev(win)))
				win=win-bgsub
				x_lo,x_hi,y_lo,y_hi=p2
				fixed=psf_fix(win,kernel,False)+bgsub
				img_nancor2[x_lo:x_hi,y_lo:y_hi] = fixed
		output_fits(img_nancor2,hdr,det_out+fl,clobber=True)


bar.finish()

print "Calibration Complete"