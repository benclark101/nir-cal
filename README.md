# NIR-Cal

A calibration routine that will reduce Raw FITS images from Near-Infrared Telescopes

## Requirements

Python 2.7
You'll also need the following libraries:
* [pyFITS](http://www.stsci.edu/institute/software_hardware/pyfits)
* [NumPy](http://www.numpy.org/)
* [Astropy](https://www.astropy.org/)
* Pandas
* math
* os
* [matplotlib](http://matplotlib.org/)
* [SciPy](http://www.scipy.org/)


## Usage

Download the raw python file to a destination of your choosing, and edit the "User Variables" section at the top of the .py file:

```############ USER VARIABLES ############
direc = "/data/1_imaging/cfht/w36/1.raw/" #directory containin all raw data (including calibration images)
output_dir="w36_cal" #name of output directory
newdir = False #create a new directory for outputs
master_table = "datamaster.cfht" #output data log name
nstars = 20 #maximum number of stars for detection algorithm
row_correction=False#Correct Bad row values (darker streaks) - DANGER: think this does bad stuff
debug_short_test=False #only reduce 10 science images if True
sci_ob="WASP-36"#the  target object keywork
skipimcor=False#skip science image correction if already performed
ap_size_lim=200 #minimum npix for valid aperture
ap_mult=2#amount to multiply mean aperture radius for "aperture" pixels
sigma_psf = 4#amount bg pix clip from median
show_sources=False
remove_bad = True#removes psfs that contain nans
```
